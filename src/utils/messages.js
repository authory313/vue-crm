export default {
  'logout': 'Вы вышли из системы',
  'auth/user-not-found': "Пользователь с таким email не существует",
  "auth/INVALID_PASSWORD": "Пароль неверный, попробуйте снова"
}

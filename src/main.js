import Vue from 'vue'
import { Vuelidate } from "vuelidate"
import App from './App.vue'
import dateFilter from './filters/date.filter'
import './registerServiceWorker'
import router from './router'
import store from './store'
import messagePlugin from './utils/message.plugin'
import LoaderView from './components/App/LoaderView.vue'

import { initializeApp } from "firebase/app";
import 'firebase/auth'
import 'firebase/database'
import { getAuth, onAuthStateChanged } from 'firebase/auth'
import currencyFilter from './filters/currency.filter'

const firebaseConfig = {
  apiKey: "AIzaSyBpydDIu-XzO7ZZTbDh14FQp5Y4XKqp4Xc",
  authDomain: "crm-vue-89285.firebaseapp.com",
  projectId: "crm-vue-89285",
  storageBucket: "crm-vue-89285.appspot.com",
  messagingSenderId: "147555249726",
  appId: "1:147555249726:web:70b97c4abe8c8bd5bf6b6f"
};

const app = initializeApp(firebaseConfig);

Vue.config.productionTip = false

Vue.use(Vuelidate)
Vue.use(messagePlugin)
Vue.filter('date', dateFilter)
Vue.filter('currencyFilter', currencyFilter)
Vue.component('loaderView',LoaderView)

const auth = getAuth()
let AppRoot
auth.onAuthStateChanged(() => {
  if (!AppRoot) {
    AppRoot = new Vue({
      Vuelidate,
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})


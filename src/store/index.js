import Vue from 'vue'
import Vuex from 'vuex'
import auth from "./auth"
import info from "./info"
import caterogy from "./caterogy"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    error: null
  },
  actions: {
    async fetchCurrency() {
      // try {
      //   const key = process.env.VUE_APP_FIXER
      //   const headers = new Headers();
      //   headers.append("apikey", key);
      //   const res = await fetch(`https://api.apilayer.com/fixer/latest?symbols=USD,EUR,RUB`, {
      //     method: "GET",
      //     redirect: "follow",
      //     headers
      //   })
      //   return await res.json()
      // } catch (error) {
      //   console.log(error);
      // }
    }
  },
  getters: {
  },
  mutations: {
    setError(state, err) {
      state.error = err
    },

    cleanError(state) {
      state.error = null
    }
  },
  getters: {
    error: state => state.error
  },
  modules: {
    auth,
    info,
    caterogy
  }
})

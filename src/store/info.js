import { getDatabase, ref, get, child } from 'firebase/database'

export default {
  state: {
    info: []
  },

  mutations: {
    setInfo(state, info) {
      state.info = info
    },

    clearInfo(state) {
      state.info = {}
    }
  },

  actions: {
    async fetchInfo({ dispatch, commit }) {
      const uid = await dispatch('getUId')

      try {
        const dbRef = ref(getDatabase())
        const info = await get(child(dbRef, `users/${uid}/info/`))
        if (info.exists()) {
          commit('setInfo', info.val())
        }
      } catch (error) {
        console.log(error);
      }
    }
  },

  getters: {
    info: (s) => s.info
  }
}

import { child, getDatabase, push, ref, onValue } from "firebase/database"

export default {
  actions: {
    async fetchCategories({ commit, dispatch },) {
      try {
        const uid = await dispatch('getUId')
        const db = getDatabase()
        const cats = []
        onValue(ref(db, `/users/${uid}/categories/`), (snapshot) => {
          const categories = snapshot.val() || {};
          Object.keys(categories).forEach((key) => {
            cats.push({
              title: categories[key].title,
              limit: categories[key].limit,
              id: key
            })
          })
        }, {
          onlyOnce: true
        });

        return cats

      } catch (error) {
        console.log(error);
      }
    },

    async createCategory({ commit, dispatch }, { title, limit }) {
      try {
        const uid = await dispatch('getUId')
        const dbRef = ref(getDatabase())
        const info = await push(child(dbRef, `/users/${uid}/categories/`), { title, limit })
        return { title, limit, id: info.key }
      } catch (error) {
        commit('setError', error)
        throw error
      }
    }
  }
}

import { getAuth, signInWithEmailAndPassword, signOut, createUserWithEmailAndPassword } from "firebase/auth"
import { getDatabase, ref, set } from 'firebase/database'

export default {
  actions: {
    async login({ dispatch, commit }, { email, password }) {
      try {
        const auth = getAuth()
        await signInWithEmailAndPassword(auth, email, password)
      } catch (error) {
        commit('setError', error)
        throw e
      }
    },

    async logout({ commit }) {
      try {
        const auth = getAuth()
        await signOut()
        commit('clearInfo')
      } catch (error) {

      }
    },

    async register({ dispatch, commit }, { email, password, name }) {
      try {
        const auth = getAuth()
        await createUserWithEmailAndPassword(auth, email, password)
        const uid = await dispatch('getUId')
        const DB = getDatabase()
        const currentUser = ref(DB, `users/${uid}/info`)
        set(currentUser, {
          bill: 100,
          name
        })
      } catch (error) {
        commit('setError', error)
        throw e
      }
    },

    async getUId() {
      const auth = getAuth()
      const user = auth.currentUser

      return user ? user.uid : null
    }
  }
}
